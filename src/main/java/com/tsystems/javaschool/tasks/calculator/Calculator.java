package com.tsystems.javaschool.tasks.calculator;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    public static String evaluate(String statement) {
        return new Object() {
            int openBkt = 0;
            int closeBkt = 0;
            int pos = -1, ch;

            void nextChar() {
                ch = (++pos < statement.length()) ? statement.charAt(pos) : -1;
            }

            boolean eat(int charToEat) {
                while (ch == ' ') nextChar();
                if (ch == charToEat) {
                    if (charToEat == '('){
                        openBkt++;
                    } else if(charToEat == ')'){
                        closeBkt++;
                    }
                    nextChar();
                    return true;
                }
                return false;
            }

            String parse() {
                try{
                    nextChar();
                    double x = parseExpression();
                    if (pos < statement.length()) throw new RuntimeException("Unexpected: " + (char)ch);
                    if (Double.isInfinite(x) || this.openBkt != this.closeBkt
                            || statement.contains("++") || statement.contains("--")
                            || statement.contains("**") || statement.contains("//")) {
                        return null;
                    }
                    if (x % 1 == 0) {
                        //целое
                        int thisX = (int) x;
                        return "" + thisX;
                    }
                    return "" + x;
                } catch (Exception e){
                    return null;
                }
            }

            double parseExpression() {
                double x = parseTerm();
                for (;;) {
                    if      (eat('+')) x += parseTerm(); // addition
                    else if (eat('-')) x -= parseTerm(); // subtraction
                    else return x;
                }
            }

            double parseTerm() {
                double x = parseFactor();
                for (;;) {
                    if      (eat('*')) x *= parseFactor(); // multiplication
                    else if (eat('/')) x /= parseFactor(); // division
                    else return x;
                }
            }

            double parseFactor() {
                if (eat('+')) return parseFactor(); // unary plus
                if (eat('-')) return -parseFactor(); // unary minus

                double x;
                int startPos = this.pos;
                if (eat('(')) { // parentheses
                    x = parseExpression();
                    eat(')');
                } else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
                    while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
                    x = Double.parseDouble(statement.substring(startPos, this.pos));
                } else {
                    throw new RuntimeException("Unexpected: " + (char)ch);
                }

                if (eat('^')) x = Math.pow(x, parseFactor()); // exponentiation

                return x;
            }
        }.parse();
    }

    public static void main(String[] args) {
        System.out.println(evaluate("(1 + 38) * 4.5 - 1 / 2."));
    }

}
