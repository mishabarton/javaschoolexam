package com.tsystems.javaschool.tasks.pyramid;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {

        try {
            try {
                Collections.sort(inputNumbers);
            } catch (OutOfMemoryError e) {
                throw new CannotBuildPyramidException();
            }
            //Collections.sort(inputNumbers);
            int[] myArray = inputNumbers.stream().mapToInt(i -> i).toArray();
            int sz = inputNumbers.size();

            ArrayList<int[]> myList = new ArrayList<>();

            int curNum = 0;
            int readNum = 0;

            while (readNum < sz) {
                int[] curArr = new int[curNum + 1];
                for (int j = 0; j <= curNum; j++) {
                    curArr[j] = myArray[readNum];
                    readNum++;
                }
                myList.add(curArr);
                curNum++;
            }

            int[][] arrayD = new int[curNum][curNum * 2 - 1];

            int zeroes = curNum - 1;

            for (int i = 0; i < myList.size(); i++) {
                int[] arr = myList.get(i);
                int indJ = 0;
                for (int j = 0; j < arr.length; j++) {
                    arrayD[i][zeroes + indJ] = arr[j];
                    indJ = indJ + 2;
                }
                zeroes--;
            }

            return arrayD;
        }catch (OutOfMemoryError | Exception e) {
            throw new CannotBuildPyramidException();
        }

    }
}