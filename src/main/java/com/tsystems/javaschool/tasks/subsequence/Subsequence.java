package com.tsystems.javaschool.tasks.subsequence;

import java.util.List;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {

        if (x == null || y == null){
            throw new IllegalArgumentException();
            //return false;
        }

        if (x.isEmpty()){
            return true;
        }
        if (y.isEmpty()){
            return false;
        }

        boolean isCorrect = false;
        int jInd = 0;

        for (int i = 0; i < x.size(); i++){
            Object cur = x.get(i);
            isCorrect = false;
            for (int j = jInd; j < y.size(); j++){
                jInd++;
                if (cur == y.get(j)){
                    isCorrect = true;
                    break;
                }
            }
            if (!isCorrect){
                break;
            }
        }

        return isCorrect;
    }
}
